# Watch logs of other containers via [logspout](https://github.com/gliderlabs/logspout)

You can ignore logs from containers by setting `LOGSPOUT` env to `ignore`.

```yaml
foo:
  environment:
    LOGSPOUT: ignore
```

You can either use curl in terminal to watch logs or view logs via another
container using ie.: [WeaveScope](../weavescope-1.0-single-node/README.md) logs
view.

```sh
curl --silent 127.0.0.1:8000/logs
```

```yaml
logspout-output:
  image: alpine:3.4
  command: |
    sh -c "
      apk --no-cache add curl
      exec curl --silent logspout/logs
    "
  environment:
    LOGSPOUT: ignore
  links:
  - logspout:logspout
```
