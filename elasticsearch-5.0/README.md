# Elasticsearch

Cluster of Elasticsearch with [specialized nodes](https://www.elastic.co/guide/en/elasticsearch/reference/5.0/modules-node.html#node-ingest-node) connected via DNS discovery.

You can limit used memory usage via `ES_JAVA_OPTS`

```yaml
elasticsearch:
  environment:
    ES_JAVA_OPTS: -Xms512M -Xmx512M
```
